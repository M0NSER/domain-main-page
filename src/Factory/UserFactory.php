<?php

declare(strict_types=1);

namespace App\Factory;

use App\Dto\RegisterUserDto;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFactory
{
    /**
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $userPasswordHasher;

    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    /**
     * @param RegisterUserDto $registerUserDto
     *
     * @return User
     */
    public function create(RegisterUserDto $registerUserDto): User
    {
        $user = new User();
        $user
            ->setEmail($registerUserDto->getEmail())
            ->setNickname($registerUserDto->getNickname())
            ->setPassword($this->userPasswordHasher->hashPassword($user, $registerUserDto->getPlainPassword()));

        return $user;
    }
}