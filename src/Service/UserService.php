<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\RegisterUserDto;
use App\Exception\UserAlreadyExistsException;
use App\Factory\UserFactory;
use App\Repository\UserRepository;
use App\Util\Mapper\Mapper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserService
{
    /**
     * @var Mapper
     */
    private Mapper $mapper;

    /**
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $userPasswordHasher;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var UserFactory
     */
    private UserFactory $userFactory;

    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    public function __construct(
        Mapper                 $mapper,
        EntityManagerInterface $entityManager,
        UserFactory            $userFactory,
        UserRepository $userRepository
    )
    {
        $this->mapper = $mapper;
        $this->entityManager = $entityManager;
        $this->userFactory = $userFactory;
        $this->userRepository= $userRepository;
    }

    /**
     * @param RegisterUserDto $registerUserDto
     *
     * @return void
     * @throws UserAlreadyExistsException
     */
    public function registerUser(RegisterUserDto $registerUserDto)
    {
        $existsUsers = $this->userRepository->checkUserExists($registerUserDto);
        if ($existsUsers > 0){
            throw new UserAlreadyExistsException();
        }

        $user = $this->userFactory->create($registerUserDto);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}