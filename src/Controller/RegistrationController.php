<?php

namespace App\Controller;

use App\Dto\RegisterUserDto;
use App\Entity\User;
use App\Enum\FlashEnum;
use App\Exception\UserAlreadyExistsException;
use App\Form\RegistrationFormType;
use App\Service\UserService;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationController extends CustomAbstractController
{
    /**
     * @var UserService
     */
    private UserService $userService;

    #[Pure] public function __construct(UserService $userService, TranslatorInterface $translator)
    {
        parent::__construct($translator);
        $this->userService = $userService;
    }

    #[Route('/register', name: 'register')]
    public function register(Request $request): Response
    {
        $form = $this->createForm(RegistrationFormType::class, $registerUserDto = new RegisterUserDto());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->userService->registerUser($registerUserDto);
            } catch (UserAlreadyExistsException $exception) {
                $this->goFlash(FlashEnum::DANGER, $exception->getMessage());

                return $this->redirectToRoute('register');
            }

            return $this->redirectToRoute('main');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
