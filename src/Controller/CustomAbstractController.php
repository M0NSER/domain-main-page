<?php

declare(strict_types=1);

namespace App\Controller;

use App\Enum\FlashEnum;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;

class CustomAbstractController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param FlashEnum $flashEnum
     * @param string    $messageCode
     * @param string    ...$params
     *
     * @return void
     */
    public function goFlash(FlashEnum $flashEnum, string $messageCode, string ...$params)
    {
        $this->addFlash(
            $flashEnum->color(),
            $this->translator->trans($messageCode, $params)
        );
    }
}