<?php

declare(strict_types=1);

namespace App\Util\Mapper;

use AutoMapperPlus\AutoMapper;

class Mapper extends AutoMapper
{
    public function __construct(MapperConfigInterface $mapperConfig)
    {
        parent::__construct($mapperConfig->build());
    }
}