<?php

declare(strict_types=1);

namespace App\Util\Mapper;

use App\Dto\RegisterUserDto;
use App\Entity\User;
use AutoMapperPlus\Configuration\AutoMapperConfig;

class MapperConfig implements MapperConfigInterface
{
    /**
     * @var AutoMapperConfig
     */
    private AutoMapperConfig $config;

    public function __construct()
    {
        $this->config = new AutoMapperConfig();
    }

    /**
     * @inheritDoc
     */
    public function build(): AutoMapperConfig
    {
        return $this
            ->buildUser()
            ->config;
    }

    /**
     * @return $this
     */
    protected function buildUser(): self
    {
        $this->config
            ->registerMapping(RegisterUserDto::class, User::class);

        return $this;
    }
}