<?php

declare(strict_types=1);

namespace App\Enum;

use function Webmozart\Assert\Tests\StaticAnalysis\string;

enum FlashEnum
{
    case SUCCESS;
    case DANGER;
    case WARNING;

    public function color(): string
    {
        return match ($this) {
            self::SUCCESS => 'success',
            self::DANGER => 'danger',
            self::WARNING => 'warning',
        };
    }
}