<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;

class UserAlreadyExistsException extends Exception
{
    public function __construct()
    {
        parent::__construct("exception.userAlreadyExists.message", 400);
    }
}