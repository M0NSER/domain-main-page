<?php

namespace App\Form;

use App\Dto\RegisterUserDto;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use function Webmozart\Assert\Tests\StaticAnalysis\null;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => false,
                'attr'  => [
                    'placeholder' => 'form.login.email',
                ],
            ])
            ->add('nickname', null, [
                'label' => false,
                'attr'  => [
                    'placeholder' => 'form.login.nickname',
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type'           => PasswordType::class,
                'first_options'  => [
                    'label' => false,
                    'attr'  => [
                        'autocomplete' => 'new-password',
                        'placeholder'  => 'form.login.insertPassword',
                    ],
                ],
                'second_options' => [
                    'label' => false,
                    'attr'  => [
                        'autocomplete' => 'new-password',
                        'placeholder'  => 'form.login.repeatPassword',
                    ],
                ],
            ])
            ->add('agreeTerms', null, [
                'label' => 'form.login.acceptTerms',
            ])
            ->add('submit', SubmitType::class, [
                'label'    => 'form.login.register',
                'attr'     => [
                    'class' => 'button button-a button-big button-rouded',
                ],
                'row_attr' => [
                    'class' => 'text-center',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'label'      => false,
            'data_class' => RegisterUserDto::class,
        ]);
    }
}
